<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Data extends BaseController {

    use ResponseTrait;

    public function index() {
        // $sql = new Sql();
        $datos['email'] = 'gianpascal@gmail.com';
        $datos['nombre'] = 'giancarlo';
        $respuesta = $this->sql->ejecutar($datos);

        return $this->setResponseFormat('json')->respond($respuesta, 200);
    }

    public function ejemplo() {
        $data = array(
            "foo" => "bar",
            "bar" => "foo",
        );
        return $this->setResponseFormat('json')->respond($data, 200);
    }

    public function post() {
        $request = service('request');
        $store = $request->getHeader('sp');
        $usuario = $request->getHeader('usuario')->getValue();
        $agente = $request->getHeader('agente')->getValue();
        $sp = $store->getValue();
        $body = $request->getPost();
            //$body='';
        // var_dump($body);
        $respuesta = $this->sql->ejecutar($sp, $body,$usuario,$agente);
        
        return $this->setResponseFormat('json')->respond($respuesta, 200);
    }

    //--------------------------------------------------------------------
}
