/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  garroyo
 * Created: 03-jul-2020
 */
 
CREATE SCHEMA ecom;

DROP TABLE ecom.usuarioWeb ;
use simedh go 
CREATE TABLE ecom.usuarioWeb 
    (
     idUsuario INTEGER IDENTITY(0, 1)NOT NULL , 
     nombres VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS , 
     apellidoPaterno VARCHAR (100) , 
     apellidoMaterno VARCHAR (100) , 
     email VARCHAR (100) , 
     documento VARCHAR (100) , 
     clave VARCHAR (100) , 
     bEstado BIT DEFAULT (1) , 
     vUsuario VARCHAR (500) DEFAULT user_name() , 
     vHost VARCHAR (25) DEFAULT host_name() , 
     dCreacion DATETIME DEFAULT getdate() , 
     cIdTipoDocumento CHAR (4),
     cTipoPersona char(1),
     bsexo char(1),
     cEstadoCivil char(4),
     dFechaNacimiento datetime
    )
    ON[primary];

ALTER TABLE ecom.usuarioweb ADD constraint usuarioweb_pk PRIMARY KEY CLUSTERED (idUsuario)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) ;



ALTER TABLE ecom.usuarioWeb 
    ADD CONSTRAINT usuarioWeb_nsmTipoDocumentos_FK FOREIGN KEY 
    ( 
     cIdTipoDocumento
    ) 
    REFERENCES dbweb.nsmTipoDocumentos 
    ( 
     cIdTipoDocumento 
    ) 

ALTER TABLE ecom.usuarioWeb
  WITH CHECK ADD  CONSTRAINT [fkk_nsmPersonas_nsmEstadoCivil] FOREIGN KEY([cEstadoCivil])
REFERENCES [dbweb].[nsmEstadosCiviles] ([cIdEstadoCivil])
    ON DELETE NO ACTION 
    ON UPDATE no action ;


ALTER TABLE ecom.usuarioWeb
ADD vUbigeo varchar(6) NULL


ALTER TABLE ecom.usuarioWeb
ADD vAgente varchar(500) NULL

ALTER TABLE ecom.usuarioWeb
ADD vCelular varchar(50) NULL

ALTER TABLE ecom.usuarioWeb
ADD vFijo varchar(50) NULL

ALTER TABLE ecom.usuarioWeb
ADD vAnexo varchar(50) NULL

CREATE TABLE ecom.pacienteUsuario (
  idPacienteUsuario int IDENTITY(0, 1) NOT NULL,
  idUsuario int NULL,
  c_cod_per char(7) NULL,
  bEstado bit NULL,
  vUsuario varchar(500) NULL,
  vHost varchar(500) NULL,
  dFechaCreacion datetime NULL,
  vAgente varchar(500) NULL,
  PRIMARY KEY CLUSTERED (idPacienteUsuario)
)

ALTER TABLE ecom.pacienteUsuario
ADD CONSTRAINT fk_paciente_usuario_idUsuario FOREIGN KEY (idUsuario) 
  REFERENCES ecom.usuarioWeb (idUsuario) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION

ALTER TABLE ecom.pacienteUsuario
ADD CONSTRAINT fk_pacienteUsuario_c_cod_per FOREIGN KEY ([c_cod_per]) 
  REFERENCES dbweb.nsmPersonas ([c_cod_per]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION

ALTER TABLE ecom.pacienteUsuario
ADD bPuedeEditar bit NULL