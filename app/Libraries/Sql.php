<?php

namespace App\Libraries;

Class Sql {

    var $con;

    function __construct() {
        $config = new \Config\App();
        $congig_simedh['dbname'] = $config->dbname;
        $congig_simedh['dbuser'] = $config->dbuser;
        $congig_simedh['dbpasw'] = $config->dbpasw;
        $congig_simedh['dbhost'] = $config->dbhost;
        $congig_simedh['dbdriv'] = $config->dbdriv;


        $serverName = $congig_simedh['dbhost']; //serverName\instanceName, portNumber (por defecto es 1433)
        $connectionInfo = array("Database" => $congig_simedh['dbname'],
            "UID" => $congig_simedh['dbuser'],
            "PWD" => $congig_simedh['dbpasw']);
        $this->con = sqlsrv_connect($serverName, $connectionInfo);

        if (!$this->con) {
            echo "Conexión no se pudo establecer.<br />";
            echo sqlsrv_errors();
            //die(print_r(sqlsrv_errors(), true));
        }
    }

    function ejecutar($sp, $body,$usuario,$agente) {
        $array['msj'] = array();
        $array['stm'] = array();
        $array['sql'] = '';

        $query = "EXECUTE ecom.$sp ";
        $cantidadParametros = count($body);
        $i=1;
        foreach ($body as $key => $value) {
            //    echo "cant $cantidadParametros - key $key <br>";
            if ($cantidadParametros > $i) {
                $query = $query . "'$value',";
            } else {
                $query = $query . "'$value'";
            }
            $i++;
        }
        if($i==1){
            $query=$query."'$usuario','$agente'";
        }else{
            $query=$query.",'$usuario','$agente'";
        }
        
        $array['sql'] =$query;
        $stmt = sqlsrv_query($this->con, $query);
        $error = sqlsrv_errors();
        $isArray=0;
        if ($error !== null) {
            ///var_dump($error);
            $array['msj'][0] = array("estado"=>0,"mensaje"=>"Error interno de sistema","query"=>$query);
            
            $array['error'] = $error;
        } else {
            $i = 0;

            do {

                while ($f = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {

                    
                    if (isset($f['mensaje'])) {
                        array_push($array['msj'], $f);
                    } else {
                        if (!isset($array['stm'][$i])) {
                            $array['stm'][$i] = array();
                        }

                        array_push($array['stm'][$i], $f);
                    }
                    $isArray=1;
                };
                if ($isArray==1){
                    $i++;
                    $isArray=0;
                }else{
                    $isArray=0;
                }
                
                $next_result = sqlsrv_next_result($stmt);
                $error = sqlsrv_errors();
                if ($error !== null) {
                    $array['error'][] = $error;
                }
            } while ($next_result);
        }
        //  print_r($array);


        return $array;
    }

}
